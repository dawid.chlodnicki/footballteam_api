<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('remember-password', 'AuthController@rememberPassword');
    Route::post('reset-password', 'AuthController@resetPassword');
    Route::get('posts', 'PostController@index');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['middleware' => 'role:editor|admin'], function () {
            Route::get('post/{id}', 'PostController@view');
            Route::post('post', 'PostController@create');
            Route::put('post/{id}', 'PostController@update');
            Route::delete('post/{id}', 'PostController@delete');
        });

        Route::group(['middleware' => 'role:admin'], function () {
            Route::get('users', 'UserController@index');
            Route::get('user/{id}', 'UserController@view');
            Route::post('user', 'UserController@create');
            Route::put('user/{id}', 'UserController@update');
            Route::delete('user/{id}', 'UserController@delete');
            Route::put('user/change-role/{id}', 'UserController@changeRole');
        });
    });
});
