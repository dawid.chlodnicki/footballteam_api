<?php

use App\Enums\RoleEnum;
use App\Role;
use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        Schema::create(
            'roles',
            function (Blueprint $table) {
                $table->id();
                $table->string('slug');
                $table->timestamps();
            }
        );

        foreach (RoleEnum::getConstants() as $slug) {
            $role = new Role();
            $role->slug = $slug;
            $role->saveOrFail();
        }

        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->foreignId('role_id')->after('password')->constrained('roles')->onDelete('cascade');
            }
        );

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@test.com';
        $user->password = Hash::make('12345678');
        $user->role_id = Role::query()->where(['slug' => RoleEnum::ADMIN])->firstOrFail()->id;
        $user->saveOrFail();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
