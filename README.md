## Installation

1. Copy *.env* from *.env.example* and set the database and mail client configuration.
2. Run commands:
    1. `composer install`
    2. `php artisan key:generate`
    3. `php artisan migrate`
3. Run a worker: `php artisan queue:work`

## API Endpoints

### `POST` /api/v1/login
##### Description
Allows users to log in with role type editor or admin.

**Admin credentials:** `admin@test.com:12345678`
##### Request headers
| Key | Value |
| ------ | ------ |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| email | `string` | - |
| password | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `400` | Invalid email or password |
___
### `POST` /api/v1/register
##### Description
Allows users to register with the default role - user.
##### Request headers
| Key | Value |
| ------ | ------ |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| name | `string` | - |
| email | `string` | - |
| password | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `422` | Incorrect data |
___
### `POST` /api/v1/remember-password
##### Description
Sends an email with remember token.
##### Request headers
| Key | Value |
| ------ | ------ |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| email | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found user |
| `422` | Incorrect data |
___
### `POST` /api/v1/reset-password
##### Description
Sets a new password.
##### Request headers
| Key | Value |
| ------ | ------ |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| remember_token | `string` | - |
| password | string | - |
| password_confirmation | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `400` | Invalid remember token |
| `422` | Incorrect data |
___
### `GET` /api/v1/posts
##### Description
Returns a list of posts.
##### Request headers
| Key | Value |
| ------ | ------ |
| Accept | application/json |
##### Query params
| Param | Type | Scope |
| ------ | ------ | ------ |
| pageNumber | `int` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
___
### `GET` /api/v1/post/{id}
##### Description
Returns data about a post. Editor or admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
___
### `POST` /api/v1/post
##### Description
Creates a new post. Editor or admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| name | `string` | - |
| content | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `422` | Incorrect data |
___
### `PUT` /api/v1/post/{id}
##### Description
Updates a post. Editor or admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| name | `string` | - |
| content | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
| `422` | Incorrect data |
___
### `DELETE` /api/v1/post/{id}
##### Description
Deletes a post. Editor or admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
___
### `GET` /api/v1/users
##### Description
Returns a list of users. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Query params
| Param | Type | Scope |
| ------ | ------ | ------ |
| pageNumber | `int` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
___
### `GET` /api/v1/user/{id}
##### Description
Returns data about a user. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
___
### `POST` /api/v1/user
##### Description
Creates a new user. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| name | `string` | - |
| email | `string` | - |
| password | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `422` | Incorrect data |
___
### `PUT` /api/v1/user/{id}
##### Description
Updates a user. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| name | `string` | - |
| email | `string` | - |
| password | `string` | - |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
| `422` | Incorrect data |
___
### `DELETE` /api/v1/user/{id}
##### Description
Deletes a user. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `404` | Not found post |
___
### `PUT` /api/v1/user/change-role/{id}
##### Description
Changes user role. Admin role required.
##### Request headers
| Key | Value |
| ------ | ------ |
| Authorization | Bearer {access_token} |
| Accept | application/json |
##### Request body
| Param | Type | Scope |
| ------ | ------ | ------ |
| roleSlug | `string` | `user`, `editor`, `admin` |
##### Response status
| Code | Description |
| ------ | ------ |
| `200` | Success |
| `400` | User not found or role out of scope |
