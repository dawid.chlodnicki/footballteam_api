<?php


namespace App\Services;


use App\Enums\RoleEnum;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRememberPasswordRequest;
use App\Http\Requests\AuthResetPasswordRequest;
use App\Jobs\SendRememberPasswordEmail;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Throwable;

class AuthService extends BaseService
{
    /**
     * @param  AuthLoginRequest  $request
     * @return array|null
     * @throws Throwable
     */
    public static function login(AuthLoginRequest $request): ?array
    {
        $user = User::query()->where(['email' => $request->input('email')])->first();

        if ($user === null) {
            return null;
        }

        $checkRole = in_array($user->role->slug, [RoleEnum::EDITOR, RoleEnum::ADMIN], true);
        $checkPassword = Hash::check($request->input('password'), $user->password);

        if (!$checkRole || !$checkPassword) {
            return null;
        }

        $accessToken = Str::random(60);
        $user->access_token = $accessToken;
        $user->saveOrFail();

        return ['access_token' => $accessToken];
    }

    /**
     * @param  AuthRememberPasswordRequest  $request
     * @return bool
     * @throws Throwable
     */
    public static function rememberPassword(AuthRememberPasswordRequest $request): bool
    {
        $user = User::query()->where(['email' => $request->input('email')])->first();

        if ($user === null) {
            return false;
        }

        $rememberToken = Str::random(60);
        $user->remember_token = $rememberToken;
        $user->saveOrFail();

        SendRememberPasswordEmail::dispatch($user);

        return true;
    }

    /**
     * @param  AuthResetPasswordRequest  $request
     * @return bool
     * @throws Throwable
     */
    public static function resetPassword(AuthResetPasswordRequest $request): bool
    {
        $user = User::query()->where(['remember_token' => $request->input('remember_token')])->first();

        if ($user === null) {
            return false;
        }

        $user->password = Hash::make($request->input('password'));
        $user->saveOrFail();

        return true;
    }
}
