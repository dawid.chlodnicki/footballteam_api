<?php


namespace App\Services;


abstract class BaseService
{
    /** @var int */
    protected const PER_PAGE = 10;
}
