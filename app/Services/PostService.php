<?php


namespace App\Services;


use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Post;
use Exception;
use Illuminate\Http\Request;
use Throwable;

class PostService extends BaseService
{
    /**
     * @param  int  $pageNumber
     * @return array
     */
    public static function index(int $pageNumber): array
    {
        $query = Post::query();
        $query->with('editor');
        $query->offset(($pageNumber - 1) * self::PER_PAGE);
        $query->limit(self::PER_PAGE);

        return $query->get()->toArray();
    }

    /**
     * @param  int  $id
     * @return Post|null
     */
    public static function view(int $id): ?Post
    {
        return Post::query()->with('editor')->find($id);
    }

    /**
     * @param  PostCreateRequest  $request
     * @return Post|null
     * @throws Throwable
     */
    public static function create(PostCreateRequest $request): ?Post
    {
        $post = new Post();

        return self::createOrUpdate($request, $post);
    }

    /**
     * @param  PostUpdateRequest  $request
     * @param  int  $id
     * @return Post|null
     * @throws Throwable
     */
    public static function update(PostUpdateRequest $request, int $id): ?Post
    {
        $post = Post::query()->find($id);

        if ($post === null) {
            return null;
        }

        return self::createOrUpdate($request, $post);
    }

    /**
     * @param  int  $id
     * @return bool
     * @throws Exception
     */
    public static function delete(int $id): bool
    {
        $user = Post::query()->find($id);

        if ($user === null) {
            return false;
        }

        return $user->delete();
    }

    /**
     * @param  Request  $request
     * @param  Post  $post
     * @return Post|null
     * @throws Throwable
     */
    private static function createOrUpdate(Request $request, Post $post): ?Post
    {
        $post->name = $request->input('name');
        $post->content = $request->input('content');
        $post->editor_id = 1;
        $post->saveOrFail();

        return $post;
    }
}
