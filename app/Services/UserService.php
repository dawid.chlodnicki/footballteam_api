<?php


namespace App\Services;


use App\Enums\RoleEnum;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Role;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use ReflectionException;
use Throwable;

class UserService extends BaseService
{
    /**
     * @param  int  $pageNumber
     * @return array
     */
    public static function index(int $pageNumber): array
    {
        $query = User::query();
        $query->with('role');
        $query->offset(($pageNumber - 1) * self::PER_PAGE);
        $query->limit(self::PER_PAGE);

        return $query->get()->toArray();
    }

    /**
     * @param  int  $id
     * @return User|null
     */
    public static function view(int $id): ?User
    {
        return User::query()->with('role')->find($id);
    }

    /**
     * @param  UserCreateRequest  $request
     * @return User|null
     * @throws Throwable
     */
    public static function create(UserCreateRequest $request): ?User
    {
        $user = new User();
        $user->role_id = Role::query()->where(['slug' => RoleEnum::USER])->firstOrFail()->id;
        return self::createOrUpdate($request, $user);
    }

    /**
     * @param  UserUpdateRequest  $request
     * @param  int  $id
     * @return User|null
     * @throws Throwable
     */
    public static function update(UserUpdateRequest $request, int $id): ?User
    {
        $user = User::query()->find($id);

        if ($user === null) {
            return null;
        }

        return self::createOrUpdate($request, $user);
    }

    /**
     * @param  int  $id
     * @return bool
     * @throws Exception
     */
    public static function delete(int $id): bool
    {
        $user = User::query()->find($id);

        if ($user === null) {
            return false;
        }

        return $user->delete();
    }

    /**
     * @param  int  $id
     * @param  string  $roleSlug
     * @return bool
     * @throws ReflectionException
     */
    public static function changeRole(int $id, string $roleSlug): bool
    {
        $user = User::query()->find($id);

        if ($user === null) {
            return false;
        }

        if (!in_array($roleSlug, RoleEnum::getConstants(), true)) {
            return false;
        }

        $user->role_id = Role::query()->where(['slug' => $roleSlug])->firstOrFail()->id;

        return $user->save();
    }

    /**
     * @param  Request  $request
     * @param  User  $user
     * @return User
     * @throws Throwable
     */
    private static function createOrUpdate(Request $request, User $user): User
    {
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->saveOrFail();

        return $user;
    }
}
