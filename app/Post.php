<?php

namespace App;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Post
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property int $editor_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static Builder|Post query()
 * @method static Builder|Post whereContent($value)
 * @method static Builder|Post whereCreatedAt($value)
 * @method static Builder|Post whereEditorId($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post whereName($value)
 * @method static Builder|Post whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read User|null $editor
 */
class Post extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'editor_id'
    ];

    /**
     * @return HasOne
     */
    public function editor(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'editor_id');
    }
}
