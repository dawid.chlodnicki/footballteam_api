<?php


namespace App\Http\Controllers;


use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Services\PostService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class PostController extends BaseController
{
    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $pageNumber = $request->query('pageNumber', 1);
        $posts = PostService::index($pageNumber);

        return $this->getSuccessResponse($posts);
    }

    /**
     * @param  int  $id
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $post = PostService::view($id);

        if ($post !== null) {
            return $this->getSuccessResponse($post);
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  PostCreateRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function create(PostCreateRequest $request): JsonResponse
    {
        $post = PostService::create($request);

        return $this->getSuccessResponse($post);
    }

    /**
     * @param  PostUpdateRequest  $request
     * @param  int  $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(PostUpdateRequest $request, int $id): JsonResponse
    {
        $user = PostService::update($request, $id);

        if ($user !== null) {
            return $this->getSuccessResponse($user);
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  int  $id
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(int $id): JsonResponse
    {
        if (PostService::delete($id)) {
            return $this->getSuccessResponse();
        }

        return $this->getNotFoundResponse();
    }
}
