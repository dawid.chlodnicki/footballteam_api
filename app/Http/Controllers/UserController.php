<?php


namespace App\Http\Controllers;


use App\Events\UserCreated;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use ReflectionException;
use Throwable;

class UserController extends BaseController
{
    /**
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $pageNumber = $request->query('pageNumber', 1);
        $users = UserService::index($pageNumber);

        return $this->getSuccessResponse($users);
    }

    /**
     * @param  int  $id
     * @return JsonResponse
     */
    public function view(int $id): JsonResponse
    {
        $user = UserService::view($id);

        if ($user !== null) {
            return $this->getSuccessResponse($user);
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  UserCreateRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function create(UserCreateRequest $request): JsonResponse
    {
        $user = UserService::create($request);

        UserCreated::dispatch($user);

        return $this->getSuccessResponse($user);
    }

    /**
     * @param  UserUpdateRequest  $request
     * @param  int  $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function update(UserUpdateRequest $request, int $id): JsonResponse
    {
        $user = UserService::update($request, $id);

        if ($user !== null) {
            return $this->getSuccessResponse($user);
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  int  $id
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(int $id): JsonResponse
    {
        if (UserService::delete($id)) {
            return $this->getSuccessResponse();
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  Request  $request
     * @param  int  $id
     * @return JsonResponse
     * @throws ReflectionException
     */
    public function changeRole(Request $request, int $id): JsonResponse
    {
        if (UserService::changeRole($id, $request->input('roleSlug', ''))) {
            return $this->getSuccessResponse();
        }

        return $this->getBadRequestResponse();
    }
}
