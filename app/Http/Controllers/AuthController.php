<?php


namespace App\Http\Controllers;


use App\Events\UserCreated;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRememberPasswordRequest;
use App\Http\Requests\AuthResetPasswordRequest;
use App\Http\Requests\UserCreateRequest;
use App\Services\AuthService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class AuthController extends BaseController
{
    /**
     * @param  AuthLoginRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function login(AuthLoginRequest $request): JsonResponse
    {
        $accessToken = AuthService::login($request);

        if ($accessToken !== null) {
            return $this->getSuccessResponse($accessToken);
        }

        return $this->getBadRequestResponse();
    }

    /**
     * @param  UserCreateRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function register(UserCreateRequest $request): JsonResponse
    {
        $user = UserService::create($request);

        UserCreated::dispatch($user);

        return $this->getSuccessResponse($user);
    }

    /**
     * @param  AuthRememberPasswordRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function rememberPassword(AuthRememberPasswordRequest $request): JsonResponse
    {
        if (AuthService::rememberPassword($request)) {
            return $this->getSuccessResponse();
        }

        return $this->getNotFoundResponse();
    }

    /**
     * @param  AuthResetPasswordRequest  $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function resetPassword(AuthResetPasswordRequest $request): JsonResponse
    {
        if (AuthService::resetPassword($request)) {
            return $this->getSuccessResponse();
        }

        return $this->getBadRequestResponse();
    }
}
