<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

abstract class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    public function getSuccessResponse($data = null): JsonResponse
    {
        return response()->json($data);
    }

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    public function getBadRequestResponse($data = null): JsonResponse
    {
        return response()->json($data, 400);
    }

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    public function getNotFoundResponse($data = null): JsonResponse
    {
        return response()->json($data, 404);
    }

    /**
     * @param  null  $data
     * @return JsonResponse
     */
    public function getInternalErrorResponse($data = null): JsonResponse
    {
        return response()->json($data, 500);
    }
}
