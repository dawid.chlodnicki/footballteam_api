<?php

namespace App\Http\Middleware;

use App\Enums\RoleEnum;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorizationHeader = $request->header('Authorization');

        if (Str::startsWith($authorizationHeader, 'Bearer ')) {
            $accessToken = Str::substr($authorizationHeader, 7);
            $user = User::query()->where(['access_token' => $accessToken])->first();

            if ($user !== null) {
                auth()->login($user);
                return $next($request);
            }
        }

        return response('Unauthorized', 401);
    }
}
