<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @param  string  $rolesAsString
     * @return mixed
     */
    public function handle($request, Closure $next, string $rolesAsString)
    {
        $roles = explode('|', $rolesAsString);

        /** @var User $user */
        $user = auth()->user();

        if ($user !== null && in_array($user->role->slug, $roles, true)) {
            return $next($request);
        }

        return response('Forbidden', 403);
    }
}
