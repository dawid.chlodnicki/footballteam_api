<?php


namespace App\Enums;


use ReflectionClass;
use ReflectionException;

abstract class BaseEnum
{
    private static $constCacheArray;

    /**
     * @return array
     * @throws ReflectionException
     */
    public static function getConstants(): array
    {
        if (self::$constCacheArray === null) {
            self::$constCacheArray = [];
        }
        $calledClass = static::class;
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }
}
