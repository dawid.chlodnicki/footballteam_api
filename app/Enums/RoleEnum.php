<?php


namespace App\Enums;


class RoleEnum extends BaseEnum
{
    public const USER = 'user';
    public const EDITOR = 'editor';
    public const ADMIN = 'admin';
}
